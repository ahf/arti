---
title: Sample Code
---

# Examples

The following are sample projects built with Arti.

- [**Download Manager**](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/examples/gsoc2023/download-manager) is a small download manager prototype which can connect to the Tor network and get a copy of the Tor browser from there.

- [**Pt-proxy**](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/examples/gsoc2023/pt-proxy) provides an interface to run the obfs4 pluggable transport in a standalone manner, ie, instead of using obfs4 to connect to the Tor network, we can use it to connect to the Internet directly.

- [**DNS resolver**](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/examples/gsoc2023/dns-resolver) uses Tor to make a DNS over TCP request for a hostname, and get IP addresses back.

- [**Connection checker**](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/examples/gsoc2023/connection-checker) attempts to check connectivity to the Tor network through a variety of ways.
